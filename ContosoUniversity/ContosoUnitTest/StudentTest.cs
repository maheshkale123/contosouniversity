﻿using ContosoUniversity.Controllers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Web.Mvc;

namespace ContosoUnitTest
{
    [TestClass]
    public class StudentTest
    {

        //Test Cases
        [TestMethod]
        public void TestForViewWithValue0()
        {
            //Arrange
            StudentController t = new StudentController();

            //Act
            ViewResult r = t.Details(0) as ViewResult;

            //Asert
            Assert.AreEqual("Details", r.ViewName);

        }

    }
}
